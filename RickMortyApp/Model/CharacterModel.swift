//
//  Model.swift
//  RickMortyApp
//
//  Created by Dmitry Pavlov on 13.11.2019.
//  Copyright © 2019 Dmitry Pavlov. All rights reserved.
//

import Foundation

struct PagedCharacterResponse: Decodable {
    let results: [Character]
    let info: Info
}
struct Info: Decodable{
    let count: Int
    let pages: Int
    let next: String
    let prev: String
}
struct Character: Decodable, Equatable{
    let id: Int
    let name: String
    let image: String
    let status: String
    let species: String
    let gender: String
}
