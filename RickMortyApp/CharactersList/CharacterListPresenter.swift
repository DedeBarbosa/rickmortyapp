//
//  CharacterListPresenter.swift
//  RickMortyApp
//
//  Created by Dmitry Pavlov on 13.11.2019.
//  Copyright © 2019 Dmitry Pavlov. All rights reserved.
//

import Foundation

protocol CharacterListPresenterProtocol: class{
    
    func viewDidLoad()
    func getCharacterName(by index: Int) -> String
    func getCharacterImage(by character: Character) -> Data?
    func getCharacterImage(by index: Int) -> Data?
    func fetchCharacter(at intdex: Int)
    func showCharacterDetails(for index: Int)
    var charactersCount: Int {get}
    
}

class CharacterListPresenter: CharacterListPresenterProtocol{
    
   
    weak var view: CharacterListViewProtocol!
    var interactor: CharacterListInteractorProtocol!
    private var characters = [Character]()
    var router: CharacterListRouterProtocol!
    var imageDict = [Int : Data]()
    
    required init(view: CharacterListViewProtocol) {
        self.view = view
    }
    
    var charactersCount: Int{
        characters.count
    }
    
    func viewDidLoad() {
        fetchCharacter(at: 10)
    }
    
    func fetchCharacter(at index: Int){
        if index >= characters.count - 1{
            view.showActivity()
            interactor.fetchCharacters()
        }
    }
    
    func getCharacterName(by index: Int) -> String {
        characters[index].name
    }

    func getCharacterImage(by character: Character) -> Data?{
        let index = characters.firstIndex{ c in
            c == character
        }
        guard let i = index else {return nil}
        return imageDict[i]
    }
    func getCharacterImage(by index: Int) -> Data?{
        if let image = imageDict[index]{
            return image
        }
        let imagePath = characters[index].image
        interactor.getImage(by: imagePath, and: index)
        return nil
    }
    func showCharacterDetails(for index: Int) {
        if characters.indices.contains(index) {
            let character = characters[index]
            router.openCharacterListDetailsViewController(with: character)
        }
    }
}

extension CharacterListPresenter: CharacterListInteractorOutputProtocol{
    func imageDidRecive(_ data: Data, for index: Int) {
        view.reloadImageInCell(by: index, with: data)
    }
    
    func charactersDidReceive(_ characters: [Character]) {
        self.characters.append(contentsOf: characters)
        view.hideActivity()
        view.reloadData()
    }
    
    
}
