//
//  CharacterListInteractor.swift
//  RickMortyApp
//
//  Created by Dmitry Pavlov on 13.11.2019.
//  Copyright © 2019 Dmitry Pavlov. All rights reserved.
//

import Foundation

protocol CharacterListInteractorProtocol: class {
    func fetchCharacters()
    func getImage(by url: String, and index: Int)
}

protocol CharacterListInteractorOutputProtocol: class {
    func charactersDidReceive(_ characters: [Character])
    func imageDidRecive(_ data: Data, for index: Int)
}

class CharacterListInteractor {
    
    weak var presenter: CharacterListInteractorOutputProtocol!
    var networkService: NetworkServiceProtocol!
    var isFetchInProgress = false
    var totalPage = -1
    var currentPage = 0
    required init(presenter: CharacterListInteractorOutputProtocol) {
        self.presenter = presenter
    }
}

extension CharacterListInteractor: CharacterListInteractorProtocol {
    
    func fetchCharacters() {
        if !isFetchInProgress, currentPage == totalPage {
            return
        } else{
            currentPage += 1
        }
        isFetchInProgress = true
        NetworkService.shared.fetchCharactersList(page: currentPage){ [weak self] pagedCharacterResponse in
            self?.presenter.charactersDidReceive(pagedCharacterResponse.results)
            self?.totalPage = pagedCharacterResponse.info.count
            self?.isFetchInProgress = false
        }
    }
    
    func getImage(by url: String, and index: Int) {
        NetworkService.shared.getImage(by: url){ [weak self] data in
            self?.presenter.imageDidRecive(data, for: index)
        }
    }
}
