//
//  CharacterRequest.swift
//  RickMortyApp
//
//  Created by Dmitry Pavlov on 14.04.2020.
//  Copyright © 2020 Dmitry Pavlov. All rights reserved.
//

import Foundation

struct CharacterRequest {
  var path: String {
    return "characters"
  }
  
  let parameters: Parameters
  private init(parameters: Parameters) {
    self.parameters = parameters
  }
}

extension ModeratorRequest {
  static func from(site: String) -> ModeratorRequest {
    let defaultParameters = ["order": "desc", "sort": "reputation", "filter": "!-*jbN0CeyJHb"]
    let parameters = ["site": "\(site)"].merging(defaultParameters, uniquingKeysWith: +)
    return ModeratorRequest(parameters: parameters)
  }
}
